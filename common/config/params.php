<?php
return [
    'adminEmail' => 'admin@worldrun.online',
    'supportEmail' => 'admin@worldrun.online',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'user.passwordResetTokenExpire' => 3600,
    'user.rememberMeDuration' => 3600 * 24 * 30,
    'cookieDomain' => '.worldrun.lk',
    'frontendHostInfo' => 'http://world-run.loc/frontend/web',
    'backendHostInfo' => 'http://world-run.loc/backend/web',
    'uploadsRoot' => \Yii::getAlias('@uploadsRoot'),
];
