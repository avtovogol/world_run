<?php
namespace frontend\controllers;

use cabinet\entities\cabinet\Race;
use cabinet\forms\auth\LoginForm;
use cabinet\forms\auth\SignupForm;
use common\mail\services\Email;
use Yii;
use yii\db\Query;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\User;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $this->redirect(['site/race', 'id' => 9]);

    }

    /**
     * @param $id
     * @return mixed
     */
    public function actionRace($id)
    {
        $race = Race::findOne($id);
        $users = $race->getUsers()->orderBy('id')->all();
        $regForm = new SignupForm();
        $authForm = new LoginForm();
        return $this->renderPartial('view',[
            'authForm' => $authForm,
            'regForm' => $regForm,
            'race' => $race,
            'users' => $users,
        ]);
    }

    public function actionParser()
    {
//        var_dump(Url::to('/users.csv'));
//        $file = fopen(Url::to('users.csv'), "r");
//        $file = fopen("users.csv", "r");
//        var_dump($file);
//        $csv = array_map('str_getcsv', $file);
//        array_walk($csv, function(&$a) use ($csv) {
//          $a = array_combine($csv[0], $a);
//        });
        $csv = array_map('str_getcsv', file('users.csv'));
        array_shift($csv); # remove column header
//        var_dump($csv);
        $csv[972][9] = '';
        $csv[2255][2] = 'Mary';
        $csv[2519][2] = 'username';
        $csv[1144][10] = 'Украина, город Днепр, индекс 49130, ул. Донецкое шоссе 121, кв 112.';

        for ($i = 0; $i < 2520; $i++) {
            $query = new Query();
            $query->select('id')
                ->from('users')
                ->where(['email' => $csv[$i][1]]);
            if (strlen($csv[$i][9]) > 50) {
                $csv[$i][9] = '';
            }
            if ($query->all() == NULL) {
                Yii::$app->db->createCommand()
                    ->insert('users', [
                        'id' => $i+1,
                        'username' => $csv[$i][2],
                        'auth_key' => Yii::$app->security->generateRandomString(),
                        'email' => $csv[$i][1],
                        'status' => 9,
                        'created_at' => time(),
                        'updated_at' => time(),
                        'verification_token' => Yii::$app->security->generateRandomString(10),
                    ])->execute();
                Yii::$app->db->createCommand()
                    ->insert('user_profile', [
                        'id'=>$i+1,
                        'first_name' => $csv[$i][2],
                        'last_name' => $csv[$i][3],
                        'sex' => $csv[$i][4],
                        'age' => $csv[$i][5],
                        'city' => $csv[$i][9],
                        'phone' => $csv[$i][6],
                        'postal_code' => $csv[$i][7],
                        'address_delivery' => $csv[$i][10],
                        'size_costume' => $csv[$i][11],
                        'user_id' => $csv[$i][0],
                    ])->execute();
                Yii::$app->db->createCommand()
                    ->insert('auth_assignments', [
                        'item_name' => 'user',
                        'user_id' => $csv[$i][0],
                        'created_at' => time(),
                    ])->execute();
            }
        }
    }


}
