
// slider reviews
function onChangeTariff() {
    var tariff_id = $('#tariff').val();
    var id_block_gifts = '#gifts_' + tariff_id;
    $('.tariff_gifts').css({'display': 'none'});
    $('#tariff_gifts_' + tariff_id).css({'display': 'block'});
}

$(document).ready(function () {

    $(".owl-carousel").owlCarousel({
        loop: true,
        items: 1,
        margin: 130,
        stagePadding: 130,
        dots: false,
        nav: true,
        navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
        navContainer: '#customNav',
        responsive: {
            // > 0
            0: {
                dots: false,
                margin: 30,
                stagePadding: 30,
            },
            // > 768
            768: {
                margin: 130,
                stagePadding: 130,
                dots: true,
            }
        }
    });

    $('#tariff').change(function () {
        onChangeTariff();
    });
    onChangeTariff();
});

//

function showUploadModal() {

    $('#modal_upload').modal('show');

}

$(document).ready(function () {

    $('#formreg').submit(function () {
        var target = $(this).find('#tariff option:selected').data('targetId');
        ym(52210744, 'reachGoal', target);
        return true;
    });

    var target = window.location.hash;
    if (target) {
        if (target == '#modal-login') {
            $('#signin').modal('show');
            return;
        }
        scrollTo(target);
    }

    $('[data-toggle="scroll"]').on('click', function (e) {
        e.preventDefault();
        var target = $(this).data('target');
        scrollTo(target)
    });

    function scrollTo(target) {
        $('html, body').stop().animate({
            scrollTop: $(target).offset().top
        });
    }

    $('[set-tariff]').on('click', function (e) {
        e.preventDefault();
        $('#tariff').val($(this).data('id'));
    });

    function calcTime() {

        var start = new Date('{{raceStartInt}}');
        var now = new Date();
        var timeDiffVal = start.getTime() - now.getTime();
        if (timeDiffVal < 0) return;

        var timeDiff = Math.abs(timeDiffVal);
        var diffDays = Math.floor(timeDiff / (1000 * 3600 * 24));
        var diffHours = Math.floor(timeDiff / (1000 * 3600)) % 24;
        var diffMinutes = Math.floor(timeDiff / (1000 * 60)) % 60;
        var diffSeconds = Math.floor(timeDiff / 1000) % 60;


        //Секунды
        var mname = declOfNum(diffSeconds, ['секунда', 'секунды', 'секунд']);
        while (diffSeconds.toString().length < 2) diffSeconds = "0" + diffSeconds;
        $('#seconds').html(diffSeconds);
        $('#seconds-str').html(mname);

        //Минуты
        var mname = declOfNum(diffMinutes, ['минута', 'минуты', 'минут']);
        while (diffMinutes.toString().length < 2) diffMinutes = "0" + diffMinutes;
        $('#minutes').html(diffMinutes);
        $('#minutes-str').html(mname);

        //часы
        var mname = declOfNum(diffMinutes, ['час', 'часа', 'часов']);
        while (diffHours.toString().length < 2) diffHours = "0" + diffHours;
        $('#hours').html(diffHours);
        $('#hours-str').html(mname);

        //Дни
        var mname = declOfNum(diffMinutes, ['день', 'дня', 'дней']);
        $('#days').html(diffDays);
        $('#days-str').html(mname);

        var timer = setTimeout(function () {
            calcTime();
            clearTimeout(timer)
        }, 1000);
    }

    calcTime();

    function declOfNum(number, titles) {
        var numStr = number.toString();
        var lastNum = parseInt(numStr.substring(numStr.length - 1, numStr.length));
        return (number > 4 && number < 21) ? titles[2] : ((lastNum > 1) && (lastNum < 5)) ? titles[1] : ((lastNum == 0) || (lastNum > 4)) ? titles[2] : titles[0];
    }

    var counter_text = $('#counter').text().toString();
    var counter_length = counter_text.length;
    var counter_html = '';
    for (char_ind = 0; char_ind < counter_length; char_ind++)
        counter_html = counter_html + '<span>' + counter_text.charAt(char_ind) + '</span>';

    $('#counter').html(counter_html);

    $('#formreg').submit(function () {

        var goalParams =
            {
                order_price: 0,
                currency: "RUB"
            }

        $.each($("#formreg #tariff option:selected"), function () {

            goalName = $(this).attr('data-target-id');
            goalParams.order_price = $(this).attr('data-price');

        });

        ym(52210744, 'reachGoal', goalName, goalParams); // yandex metrika
        VK.Retargeting.Event(goalName); // vk

        // FB
        fbq('track', 'Lead', {
            value: goalParams.order_price,
            currency: 'rub',
        });


        gtag_report_conversion(null, $(this).attr('data-price'));
        return true;
    });
});

function showPreloader() {
    $('#preloader').css({'display': 'flex'});
}

function hidePreloader() {
    $('#preloader').hide();
}

function submitUserInfo() {
    showPreloader();
    $('#user-info').submit();
}


$('#alert').modal('show');


