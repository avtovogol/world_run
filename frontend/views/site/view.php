<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';

use cabinet\entities\cabinet\Race;
use frontend\assets\AppAsset;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

AppAsset::register($this);

?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" prefix="og: http://ogp.me/ns#">
<head>
    <meta charset="UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>

    <meta property="og:title" content="WorldRun.online - Онлайн забеги"/>
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="//worldrun.online"/>
    <meta property="og:description" content="Первый, самый масштабный онлайн забег."/>
    <meta property="og:image" content="//worldrun.online/assets_landing/img/og_logo.jpg"/>
    <meta property="twitter:card" content="summary_large_image"/>
    <?php $this->registerCsrfMetaTags() ?>

    <link rel="stylesheet" href="/assets_landing/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="/assets_landing/css/style.css"/>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css"
          integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

    <link href="/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"/>

    <title>WorldRun.online - Онлайн забеги</title>

    <script charset="UTF-8" src="//cdn.sendpulse.com/js/push/af40a0310cf09a24a62b590f202b798c_1.js" async></script>

    <style>
        .head {
            background-image: url(/assets_landing/img/back4.png) !important;
        }
    </style>
    <script type="text/javascript">!function () {
            var t = document.createElement("script");
            t.type = "text/javascript", t.async = !0, t.src = "https://vk.com/js/api/openapi.js?160", t.onload = function () {
                VK.Retargeting.Init("VK-RTRG-353907-6vGNx"), VK.Retargeting.Hit()
            }, document.head.appendChild(t)
        }();</script>
    <noscript><img src="https://vk.com/rtrg?p=VK-RTRG-353907-6vGNx" style="position:fixed; left:-999px;" alt=""/>
    </noscript>


    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'UA-143695134-2');
        gtag('config', 'AW-995161942');
    </script>

    <script>
        function gtag_report_conversion(price) {

            gtag('event', 'conversion', {
                'send_to': 'AW-995161942/hM0SCNapt6UBENbuw9oD',
                'value': price,
                'currency': 'RUB',
                'event_callback': callback
            });
            return false;
        }
    </script>

</head>
<body>
<!-- Facebook Pixel Code -->
<script>
    !function (f, b, e, v, n, t, s) {
        if (f.fbq) return;
        n = f.fbq = function () {
            n.callMethod ?
                n.callMethod.apply(n, arguments) : n.queue.push(arguments)
        };
        if (!f._fbq) f._fbq = n;
        n.push = n;
        n.loaded = !0;
        n.version = '2.0';
        n.queue = [];
        t = b.createElement(e);
        t.async = !0;
        t.src = v;
        s = b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t, s)
    }(window, document, 'script',
        'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '549868348856726');
    fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
               src="https://www.facebook.com/tr?id=549868348856726&ev=PageView&noscript=1"
    /></noscript>
<!-- End Facebook Pixel Code -->
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (m, e, t, r, i, k, a) {
        m[i] = m[i] || function () {
            (m[i].a = m[i].a || []).push(arguments)
        };
        m[i].l = 1 * new Date();
        k = e.createElement(t), a = e.getElementsByTagName(t)[0], k.async = 1, k.src = r, a.parentNode.insertBefore(k, a)
    })
    (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

    ym(52210744, "init", {
        id: 52210744,
        clickmap: true,
        trackLinks: true,
        accurateTrackBounce: true,
        webvisor: true
    });
</script>
<noscript>
    <div><img src="https://mc.yandex.ru/watch/52210744" style="position:absolute; left:-9999px;" alt=""/></div>
</noscript>
<!-- /Yandex.Metrika counter -->


<div class="modal" id="alert">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content rounded-0">
            <div class="modal-body p-4">
                <div id="alert-msg"
                     class="alert w-100 m-0 position-sticky rounded-0 z-index-1 top-0 alert-{{alert.class}}">
                    {{alert.message}}
                </div>
            </div>
        </div>
    </div>
</div>

<?php if (Yii::$app->session->hasFlash('success')): ?>
    <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                    aria-hidden="true">&times;</span></button>
        <?php echo Yii::$app->session->getFlash('success'); ?>
    </div>
<?php endif; ?>

<div class="modal" id="modal_strava">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content rounded-0">
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>


<!--section head-->
<section class="head py-5">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center col-md-6 text-md-left">
                <img src="/assets_landing/img/logo.png"/>
            </div>
            <div class="col-12 text-center col-md-6 text-md-right mt-md-0 mt-5">
                <?php if (Yii::$app->user->isGuest): ?>
                    <a data-toggle="modal" data-target="#signin" class="custom-button text-light text-decoration-none">Личный
                        кабинет</a>
                <?php else: ?>
                    <?= Html::a('Личный кабинет', Url::to('/frontend/web/cabinet/participation'), ['class' => 'custom-button text-light text-decoration-none']) ?>
                <?php endif; ?>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="lead-text text-color">Внесите свое имя в книгу рекордов проекта Worldrun.online!</div>
            </div>
            <div class="col-12">
                <div class="text-light text-center">Бегайте в течении 7 дней и загружайте ваши результаты</div>
            </div>
            <div class="col-12">
                <div class="text-light text-center">Получите диплом, крутую медаль и другие подарки</div>
            </div>
        </div>

        <?php if (count($race->userAssignments) > 100): ?>
            <div class="row">
                <div class="col-12">
                    <div class="text-light counter" id="counter"><?= count($race->userAssignments) ?></div>
                    <div class="text-24 text-light text-center">участников со всего мира</div>
                </div>
            </div>
        <?php endif; ?>
        <?php if (Yii::$app->user->isGuest): ?>
            <div class="row">
                <div class="col-12 text-center">
                    <a href="#signup" data-toggle="scroll" data-target="#signup"
                       class="custom-button text-uppercase text-light text-decoration-none">Зарегистрироваться</a>
                </div>
            </div>
        <?php endif; ?>

        <div class="row">
            <div class="col-6 text-light text-center">
                <div>Старт</div>
                <div><?= Yii::$app->formatter->asDate($race->date_start) ?></div>
            </div>
            <div class="col-6 text-light text-center">
                <div>Финиш</div>
                <div><?= Yii::$app->formatter->asDate($race->date_end) ?></div>
            </div>
        </div>

        <div class="row">
            <div class="col-12 text-light text-center">
                <?= $race->statusText ?>
            </div>
        </div>

    </div>
</section>
<!--endsection-->

<!--section how-to-->
<section class="how-to" id="how-to">
    <div class="container h-100 py-3">
        <div class="row">
            <div class="col-12">
                <div class="lead-text my-3">Как принять участие</div>
            </div>
        </div>
        <!--desktop-->
        <div class="d-none d-md-block">
            <div class="row">
                <div class="col-12 media align-items-center my-3">
                    <img class="mr-3 icon" src="/assets_landing/img/swift/1.png"/>
                    <div class="media-body ml-md-3">
                        <div class="row">
                            <div class="col-6"><b class="color-num">1. </b>Зарегистрируйтесь и получите стартовый номер
                            </div>
                            <div class="col-6 text-center"><a href="#signup" data-toggle="scroll" data-target="#signup"
                                                              class="custom-button text-light text-decoration-none">Регистрация</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 media align-items-center my-3">
                    <img class="mr-3 icon" src="/assets_landing/img/swift/2.png"/>
                    <div class="media-body ml-md-3">

                        <div class="row">
                            <div class="col-6"><b class="color-num">2. </b>Зафиксируйте результат используя любое
                                мобильное приложение:
                            </div>
                            <div class="col-6 text-center"><img class="mr-3" alt="strava"
                                                                src="/assets_landing/img/apps.png"/>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 media align-items-center my-3">
                    <img class="mr-3 icon" src="/assets_landing/img/swift/3.png"/>
                    <div class="media-body ml-md-3">
                        <div class="row">
                            <div class="col-6">
                                <b class="color-num">3. </b>Загрузите свой трек с результатом в личном кабинете
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 media align-items-center my-3">
                    <img class="mr-3 icon" src="/assets_landing/img/swift/4.png"/>
                    <div class="media-body ml-md-3">
                        <p><b class="color-num">4. </b>Получите диплом, уникальную медаль финишера и другие подарки</p>
                    </div>
                </div>
            </div>
        </div>
        <!--enddesktop-->
        <!--mobileslider-->
        <div class="d-md-none h-75">
            <div class="row h-100">
                <div class="col-12 h-100">

                    <div class="panel text-center">
                        <img class="my-3 icon" src="/assets_landing/img/swift/1.png"/>
                        <div class="panel-body">
                            <p><b class="color-num">1. </b>Зарегистрируйтесь и получите стартовый номер</p>
                            <a href="#signup" class="custom-button text-light text-decoration-none">Регистрация</a>
                        </div>
                    </div>

                </div>
                <div class="col-12 h-100">

                    <div class="panel text-center">
                        <img class="my-3 icon" src="/assets_landing/img/swift/2.png"/>
                        <div class="panel-body">
                            <p><b class="color-num">2. </b>Зафиксируйте результат используя любое мобильное приложение:
                            </p>
                            <p class="text-center"><img class="mr-3" alt="strava" src="/assets_landing/img/apps.png"/>
                            </p>

                        </div>
                    </div>

                </div>

                <div class="col-12 h-100">

                    <div class="panel text-center">
                        <img class="my-3" src="/assets_landing/img/swift/3.png"/>
                        <div class="panel-body">
                            <p>
                                <b class="color-num">3. </b>Загрузите свой трек с результатом в личном кабинете</a>
                            </p>
                        </div>
                    </div>

                </div>

                <div class="col-12 h-100">

                    <div class="panel text-center">
                        <img class="my-3 icon" src="/assets_landing/img/swift/4.png"/>
                        <div class="panel-body">
                            <p><b class="color-num">4. </b>Получите диплом и уникальную медаль финишера</p>
                        </div>
                    </div>

                </div>

            </div>
        </div>
        <!--endmobile-->
    </div>
</section>

<!--счётчик-->
<section class="date-counter py-5">
    <?php $time = strtotime($race->date_start) - time();
    if ($time > 0):?>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="lead-text mb-5">До старта осталось</div>
                </div>
                <div class="col-12 text-center" id="timer-body">
                    <span class="lead-text text-color text-bolder text-72"
                          id="days"><?= round($time / (60 * 60 * 24)) ?></span> <span
                            id="days-str">дней</span> <span class="text-72 lead-text text-color text-bolder"
                                                            id="hours"><?= round($time % (60 * 60 * 24) / (60 * 60)) ?></span>
                    <span id="hours-str">часов</span> <span
                            class="text-72 lead-text text-color text-bolder"
                            id="minutes"><?= round($time % (60 * 60 * 24) % (60 * 60) / 60) ?> </span> <span
                            id="minutes-str">минут</span>
                    <span class="text-72 lead-text text-color text-bolder"
                          id="seconds"><?= round($time % (60 * 60 * 24) % (60 * 60) % 60) ?> </span> <span
                            id="seconds-str">секунд</span>
                </div>
                <?php if (Yii::$app->user->isGuest): ?>
                    <div class="col-12 text-center">
                        <a href="#signup" data-toggle="scroll" data-target="#signup"
                           class="custom-button text-light text-decoration-none mt-5">Зарегистрироваться</a>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    <?php endif; ?>
</section>

<!--правила забега-->
<section class="instruction" id="instruction">
    <div class="container h-100 py-3">
        <div class="row">
            <div class="col-12">
                <div class="lead-text my-3">Правила забега</div>
            </div>
        </div>
        <!--desktop-->
        <div class="d-md-block">
            <div class="row">
                <div class="col-12 media align-items-center my-3">
                    <div class="media-body ">
                        <div class="row">
                            <div><b class="color-num">1. </b> Забег проводится
                                <b><?= 1 + date_diff(new DateTime($race->date_end), new DateTime($race->date_start))->format('%a') ?>
                                    дней</b> с <?= Yii::$app->formatter->asDate($race->date_start) ?> по
                                <?= Yii::$app->formatter->asDate($race->date_end) ?>.
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 media align-items-center  my-3">
                    <div class="media-body ">

                        <div class="row">
                            <div><b class="color-num">2. </b>Вы можете загружать пробежки в любой день
                                в течении проведения забега с
                                <b><?= Yii::$app->formatter->asDateTime($race->date_start, 'short') ?></b> по
                                <b><?= Yii::$app->formatter->asDateTime($race->date_end, 'short') ?></b> по МСК.
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 media align-items-center my-3">
                    <div class="media-body ">
                        <div class="row">
                            <div>
                                <b class="color-num">3. </b>Дистанция суммируется. <b>Побеждает тот</b>, кто пробежит
                                больше всех за 7 дней.
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 media align-items-center  my-3">
                    <div class="media-body ">

                        <div class="row">
                            <div><b class="color-num">4. </b>К загрузке принимаются пробежки сделанные <b>в тот же
                                    день</b>.
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 media align-items-center  my-3">
                    <div class="media-body ">

                        <div class="row">
                            <div><b class="color-num">5. </b>К загрузке принимаются только пробежки в темпе <b>7:00 на
                                    км и быстрее</b>.
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
        <!--enddesktop-->

    </div>
</section>

<!--зачем вам это-->
<section class="for-what py-5 text-center" id="for-what">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="lead-text text-light">Зачем Вам это</div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <ul class="list-group list-group-flush text-light text-left">
                    <li class="list-group-item">- Вы хотите получить крутую медаль</li>
                    <li class="list-group-item">- Вы хотите проверить себя на прочность</li>
                    <li class="list-group-item">- Вы хотите присоединиться к беговому сообществу</li>
                    <li class="list-group-item">- Вы победитель по жизни</li>
                    <li class="list-group-item">- Вы хотите стать участником соревнований</li>
                    <li class="list-group-item">- Для Вас это дополнительная мотивация</li>
                    <li class="list-group-item">- Вы хотите попробовать что то новое</li>
                </ul>
            </div>

        </div>

    </div>
</section>

<!--подарки-->
<section class="d-none d-md-block slots py-5">
    <?php if ($race->products): ?>
        <div class="container">
            <div class="row pb-5">
                <div class="col-12">
                    <div class="lead-text text-light">Выберите свой комплект</div>
                </div>
            </div>
            <div class="row text-center align-items-center justify-content-center">
                <?php foreach ($race->products as $product): ?>
                    <div class="col-4">
                        <div class="slot-panel">
                            <div class="nl">
                                <p class="text-center pt-3 text-24 text-uppercase m-0"><?= $product->name ?></p>
                                <p class="text-color text-36 m-0"><?= $product->price ?> <span
                                            class="stroke text-36">Р</span></p>
                                <?= Html::img($product->getThumbFileUrl('photo', 'thumb'), ['class' => 'img-responsive']) ?>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    <?php endif; ?>
</section>

<!--регистрация-->
<?php if (Yii::$app->user->isGuest == true or $race->getUser(Yii::$app->user->id)->all() == NULL) : ?>
    <section class="reg-panel py-5" id="signup">
        <div class="container">
            <div class="row">
                <div class="col-12 mb-3">
                    <div class="lead-text text-light">Регистрация участников</div>
                </div>
            </div>
        </div>


        <?php if (Yii::$app->user->isGuest): ?>
            <div class="container py-3">
                <div class="row z-ind">
                    <div class="col-7 text-light d-none d-md-block">
                        1. Укажите Ваше имя и адрес электронной почты<br/>
                        2. Нажмите кнопку "Зарегистрироваться"<br/>
                        3. После этого, на указанную Вами почту, вышлем дальнейшие инструкции<br/>
                        <br/>
                        <br/>
                        <br/>

                    </div>


                    <div class="text-light">
                        <?php $form = ActiveForm::begin(['id' => 'form-signup', 'enableAjaxValidation' => false, 'action' => '../signup']); ?>

                        <?= $form->field($regForm, 'raceId')->hiddenInput(['value' => $race->id])->label(' ') ?>

                        <?= $form->field($regForm, 'username')->textInput(['placeholder' => 'Имя'])->label(' ') ?>

                        <?= $form->field($regForm, 'email')->textInput(['placeholder' => 'Электронная почта'])->label(' ') ?>

                        <div class="form-group mt-3 d-flex flex-column">
                            <?= Html::submitButton('Зарегистрироваться', ['class' => ' custom-button text-light text-decoration-none w-100 m-auto', 'name' => 'signup-button']) ?>
                            <small class="text-muted text-center mt-1">Нажимая кнопку "Зарегистрироваться", Вы
                                принимаете
                                условия оферты.
                            </small>
                        </div>

                        <?php ActiveForm::end(); ?>
                    </div>

                </div>

                <div class="img-medal">
                    <img src="/assets_landing/img/medals/medal_photo716.png" class="img-responsible medal_photo"/>
                </div>
            </div>

        <?php else: ?>
            <div class="container py-3">
                <div class=" z-ind">
                    <div class="form-group mt-3 d-flex flex-column">
                        <?= Html::a('Зарегистрироваться', Url::to(['/shop/checkout', 'raceId' => $race->id]), ['class' => 'custom-button text-light text-decoration-none w-50 m-auto']) ?>
                        <small class="text-muted text-center mt-1">Нажимая кнопку "Зарегистрироваться", Вы
                            принимаете
                            условия оферты.
                        </small>
                    </div>
                </div>
            </div>

            <div class="">
                <img src="/assets_landing/img/medals/medal_photo716.png" class="img-responsible-reg "/>
            </div>
        <?php endif; ?>


    </section>
<?php endif; ?>

<!--таблица-->
<?= $this->render('_registered_users', [
    'race' => $race,
    'users' => $users,
]) ?>

<!--отзывы-->
<section class="review container">
    <div class="row ">
        <div class="col-12">
            <h2 class="lead-text my-3">Отзывы о прошлых забегах</h2>
        </div>
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                <li data-target="#carousel-example-generic" data-slide-to="3"></li>
                <li data-target="#carousel-example-generic" data-slide-to="4"></li>
                <li data-target="#carousel-example-generic" data-slide-to="5"></li>
            </ol>
            <div class="container_slider">
                <div class="carousel-inner" role="listbox">
                    <div class="carousel-item active">
                        <div class="inner-testimonial">

                            <img src="/assets_landing/review/3.jpg" class="full-opacity">
                            <h3 class="city-name">Алексеев Владимир <a href="https://vk.com/id172099578" class="vk_icon"
                                                                       target="_blank" rel="noopener nofollow"></a>, г.
                                Белгород, Инженер-строитель</h3>
                            <p class="text-testimonial">

                                Понравилась быстрая обратная связь. Я своевременно получал ответы на заданные мною
                                вопросы. Анастасия мило общалась со мной.
                                Меня сразу зацепило крутое название проекта.
                                Ребята публикуют очень интересные посты у себя в социальных сетях. Советую всем
                                подписаться. :)</p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="inner-testimonial">

                            <img src="/assets_landing/review/4.jpg" class="full-opacity">
                            <h3 class="city-name lead-text">Антошенков Дмитрий <a href="https://vk.com/id42616788"
                                                                                  class="vk_icon" target="_blank"
                                                                                  rel="noopener nofollow"></a>, 39 лет,
                                г. Жигулёвск, Региональный представитель</h3>
                            <p class="text-testimonial">
                                Хочу отметить простоту использования сайта.
                                Не трудно было понять суть этого масштабного мероприятия.
                                Всё было чётко и понятно разъяснено. Можно участвовать и получать призы.</p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="item">
                            <div class="inner-testimonial">

                                <img src="/assets_landing/review/5.jpg" class="full-opacity">
                                <h3 class="city-name">Романов Юрий <a href="https://vk.com/id164657563" class="vk_icon"
                                                                      target="_blank" rel="noopener nofollow"></a>, 37
                                    лет,
                                    г. Белгород</h3>
                                <p class="text-testimonial">
                                    Спасибо большое за организацию такого забега. С Андреем было интересно
                                    соревноваться!!!
                                    Желаю всем будущим участникам лёгких пробежек, терпения на длинных дистанциях, и
                                    конечно
                                    подальше убегайте от травм слушайте свой организм!
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="inner-testimonial">
                            <img src="/assets_landing/review/6.jpg" class="full-opacity">
                            <h3 class="city-name">Андрей Чернышов <a href="https://vk.com/id389189741" class="vk_icon"
                                                                     target="_blank" rel="noopener nofollow"></a> , 36
                                лет, г. Биробиджан, Региональный представитель</h3>
                            <p class="text-testimonial">
                                Проект worldrun.online сделал меня победителем.
                                Я не ищу лёгких путей,каждый день я убиваю в себе лень и страх.
                                Каждый день я заставлял себя идти до конца дистанции, за меня болеет мой родной город
                                Биробиджан.
                                Ставить цель идти к ней,каждый человек может это сделать, самое главное захотеть.
                                Я вставал рано утром в 4:15 и бежал,боролся сам с собой. Это принесло свои плоды.
                                Я занял первое место в соревнованиях и получил денежный приз.</p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="inner-testimonial">

                            <img src="/assets_landing/review/1.jpg" class="full-opacity">

                            <h3 class="city-name">Акберов Артур, 29 лет, г. Казань, Преподаватель физической
                                культуры.</h3>
                            <p class="text-testimonial">
                                Забег получился интересный)) Бежишь один, но знаешь, что борешься за километры, так как
                                где-то тоже бегут и тоже борются за результат.
                                У многих бегунов с разных городов и стран погода разнообразна и также разнообразны тропы
                                дистанции.
                                В Казани в эти дни было солнечно и плюсовая температура, а вот под вечер подмораживало и
                                становилось скользковато.
                                Но все боролись в разных условиях и у всех получились интересные результаты))))
                                Все молодцы и по любому получили удовольствие и заряд драйва!!! Спасибо за
                                организацию!!! 👍😃🔥🏃🏃🏃
                            </p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="inner-testimonial">

                            <img src="/assets_landing/review/2.jpg" class="full-opacity">

                            <h3 class="city-name">Борисевич Виктор, г. Барановичи, Инженер-технолог.</h3>
                            <p class="text-testimonial">
                                Мне понравилась идея забега и классная медаль.
                                Есть возможность принять участие в соревнованиях и получить денежный приз. Меня
                                привлекло бесплатное участие.
                            </p>

                        </div>
                    </div>
                </div>
            </div>
            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                <span class="icon-prev" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>

    </div>
</section>


<!--гарантии-->
<section class="container garant py-3" id="garant">
    <div class="row">
        <div class="col-12">
            <div class="lead-text my-3">Мы вам гарантируем</div>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-md-6 media align-items-center my-3">
            <img class="mr-3 icon" src="/assets_landing/img/icons/8.png"/>
            <div class="media-body ml-md-3">
                <p>Бесплатную доставку подарков почтой по России. Доставка в страны СНГ (Казахстан, Украина, Беларусь и
                    другие) оплачивается отдельно в размере 500 рублей.<br/>
                    Максимальный срок пересылки по России 14 дней.<br/>
                    Максимальный срок пересылки по СНГ 17–27 дней.
                </p>
            </div>
        </div>
        <div class="col-12 col-md-6 media align-items-center my-3">
            <img class="mr-3 icon" src="/assets_landing/img/icons/10.png"/>
            <div class="media-body ml-md-3">
                <p>Служба заботы о клиентах в период проведения забега</p>
            </div>
        </div>

        <div class="col-12 col-md-6 media align-items-center my-3">
            <img class="mr-3 icon" src="/assets_landing/img/icons/4.png"/>
            <div class="media-body ml-md-3">
                <p>Крутую медаль с уникальным дизайном (для тарифов стандарт и премиум)</p>
            </div>
        </div>

        <div class="col-12 col-md-6 media align-items-center my-3">
            <img class="mr-3 icon" src="/assets_landing/img/icons/cash_back.png"/>
            <div class="media-body ml-md-3">
                <p>Возврат денежных средств в любой момент до старта забега</p>
            </div>
        </div>
    </div>
</section>


<section id="vk-widget" class="container py-5">
    <script type="text/javascript" src="https://vk.com/js/api/openapi.js?160"></script>

    <!-- VK Widget -->
    <div class="row">
        <div class="col">
            <div>
                <div id="vk_groups" style="height: 400px;width:100%;"></div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        VK.Widgets.Group("vk_groups", {mode: 3, width: "auto", wide: 1}, 175927717);
    </script>
</section>
<section class="footer py-3">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-3 text text-center text-md-left">
                <small>© 2019 WorldRun.Online <br/>Организуем онлайн забеги</small>
            </div>
            <div class="col-12 col-md-6">
                <div class="text-center">
                    <small><a href="/agreement.pdf" rel="nofollow" target="_blank" class="text-light">Правила обработки
                            персональных данных</a></small>
                </div>
                <div class="text-center">
                    <small><a href="/oferta.pdf" rel="nofollow" target="_blank" class="text-light">Договор оферты</a>
                    </small>
                </div>
            </div>
            <div class="col-12 col-md-3">
                <div class="text-center text-md-right text-light">
                    <small>
                        По вопросам работы сервиса, сотрудничества и партнёрства:
                        <a href="mailto:admin@worldrun.online">admin@worldrun.online</a>
                    </small>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 text text-center py-3">
                <small>ИП Батурин, ИНН: 790153298180, ОГРН: 312790129000019</small>
            </div>
        </div>
    </div>
</section>
<div class="modal" id="signin">
    <div class="modal-dialog">
        <div class="modal-content rounded-0">
            <div class="modal-body">
                <div class="well">
                    <h2>Войти в личный кабинет</h2>

                    <?php $form = ActiveForm::begin(['id' => 'login-form', 'action' => '../login']); ?>

                    <?= $form->field($authForm, 'username')->textInput(['placeholder' => 'Введите email'])->label('Email') ?>

                    <?= $form->field($authForm, 'password')->passwordInput() ?>

                    <?= $form->field($authForm, 'rememberMe')->checkbox() ?>

                    <div>
                        <?= Html::submitButton('Войти', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                        <span>или <?= Html::a('Зарегистрируйтесь', Url::to(['../auth/signup/request'])) ?></span>
                    </div>

                    <div style="margin-top: 1em">
                        <?= Html::a('Забыли пароль?', Url::to(['/auth/reset/request'])) ?>
                    </div>

                    <?php ActiveForm::end(); ?>

                </div>
            </div>
        </div>
    </div>
</div>
<script src="/assets_landing/js/landing.js"></script>

<script src="/assets_landing/js/jquery-2.1.3.min.js"></script>

<script src="/assets_landing/owl-carousel/owl.carousel.min.js"></script>
<script src="/assets_landing/js/bootstrap.bundle.min.js"></script>

<link rel="stylesheet" href="/assets_landing/animate.css">
<link rel="stylesheet" href="/assets_landing/owl-carousel/owl.carousel.min.css">
<link rel="stylesheet" href="/assets_landing/owl-carousel/owl.theme.default.min.css">
<link rel="stylesheet" href="/assets_landing/font-awesome-4.6.3/css/font-awesome.min.css">


<script>
    window.ChatraSetup = {
        language: 'ru'
    };
</script>
<!-- Chatra {literal} -->
<script>
    (function (d, w, c) {
        w.ChatraID = 'eimnJrGKnfN4Wh8EL';
        var s = d.createElement('script');
        w[c] = w[c] || function () {
            (w[c].q = w[c].q || []).push(arguments);
        };
        s.async = true;
        s.src = 'https://call.chatra.io/chatra.js';
        if (d.head) d.head.appendChild(s);
    })(document, window, 'Chatra');
</script>
<!-- /Chatra {/literal} -->

<div id="preloader">
    <div class="overlap"><img width=64 height=64 src="/assets_landing/img/preloader.gif"></div>
</div>
</body>
</html>

