<?php use yii\helpers\Html;

?>
<section class="cus-table">
    <div class="container">
        <div class="row">
            <div class="col-12 py-3">
                <div class="lead-text">Зарегистрированные участники</div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 overflow-hidden">
                <div class="overflow-x-auto">
                    <table class="table table-striped">
                        <thead class="thead">
                        <tr>
                            <td>#</td>
                            <td>Старт. номер</td>
                            <td>Фамилия Имя</td>
                            <td>Город</td>
                            <td>Возраст</td>
                            <td>Пол</td>
                        </tr>
                        </thead>
                        <tbody class="tbody">
                        <?php $i = 1; foreach ($users as $user) : ?>

                            <?php
                            /** @var \cabinet\entities\cabinet\UserAssignment $assignment */
                            $startNumber=0;
                            foreach ($user->getUserAssignments()->all() as $assignment):
                                if ($assignment->race_id == $race->id) {
                                    $startNumber = $assignment->start_number;
                                    break;
                                }
                            endforeach;
                            //            var_dump($user);

                            $startNumber = str_pad($startNumber, 4, '0', STR_PAD_LEFT);
                            ?>
                            <tr>
                                <td class="text-left"><?= $i?></td>

                                <td class="text-left"><?= $startNumber ?></td>
                                <td class="text-left">
                                    <?= Html::a(Html::encode("{$user->profile->first_name}  {$user->profile->last_name}")) ?>
                                </td>
                                <td class="text-left">
                                    <?= Html::encode($user->profile->city) ?>
                                </td>
                                <td class="text-left">
                                    <?= Html::encode($user->profile->age) ?>
                                </td>
                                <td class="text-left">
                                    <?= \cabinet\helpers\ProfileHelper::sexConvertShortString($user->profile->sex) ?>
                                </td>


                            </tr>
                            <?php $i++; endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>