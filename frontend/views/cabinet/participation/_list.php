<?php

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\DataProviderInterface */

use cabinet\entities\cabinet\Race; ?>
<ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#panel1">Идёт регистрация</a></li>
    <li><a data-toggle="tab" href="#panel2">Идёт забег</a></li>
    <li><a data-toggle="tab" href="#panel3">Забег завершён</a></li>
</ul>
<div class="tab-content">
    <div id="panel1" class="tab-pane fade in active">
        <?= \yii\widgets\ListView::widget([
            'dataProvider' => $dataProviderStartReg,
            'layout' => "{items}\n{pager}",
            'itemView' => '_race',
            'itemOptions' => ['class' => 'col-sm-12 index-race'],
            'options' => ['class' => 'row'],
        ]) ?>
    </div>
    <div id="panel2" class="tab-pane fade">
        <?= \yii\widgets\ListView::widget([
            'dataProvider' => $dataProviderStartDate,
            'layout' => "{items}\n{pager}",
            'itemView' => '_race',
            'itemOptions' => ['class' => 'col-sm-12 index-race'],
            'options' => ['class' => 'row'],
        ]) ?>
    </div>
    <div id="panel3" class="tab-pane fade">
        <?= \yii\widgets\ListView::widget([
            'dataProvider' => $dataProviderFinished,
            'layout' => "{items}\n{pager}",
            'itemView' => '_race',
            'itemOptions' => ['class' => 'col-sm-12 index-race'],
            'options' => ['class' => 'row'],
        ]) ?>
    </div>
</div>
