<?php
/**
 * @var $race \cabinet\entities\cabinet\Race
 * @var $users \cabinet\entities\user\User[]
 */

use cabinet\entities\cabinet\Race;
use cabinet\entities\user\Profile;
use cabinet\entities\user\User;
use cabinet\helpers\ProfileHelper;
use cabinet\helpers\UserHelper;
use yii\helpers\Html;
use cabinet\helpers\RaceHelper;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;


$this->title = $race->name;
?>

<div class="panel panel-default">
    <div class="panel-heading"><?= $race->name ?></div>
    <div class="panel-body info"><?= $race->description ?></div>
    <div class="panel-body race-item detail">
        <div class="thumbnail">
            <?php if ($race->photo):
                echo Html::img($race->getThumbFileUrl('photo', 'thumb'), ['class' => 'img-responsive']); ?>
            <?php endif; ?>
        </div>
        <div class="info-race">
            <div class="info-text">
                <h4>Период проведения:</h4>
                <span><strong><?= date('d.m.Y', strtotime($race->date_start)) ?></strong></span> -
                <span><strong><?= date('d.m.Y', strtotime($race->date_end)) ?></strong></span>
            </div>
            <div class="info-text">
                <h4>Регистрация</h4>
                с <span><strong><?= date('d.m.Y', strtotime($race->date_reg_from)) ?></strong></span>
                по <span><strong><?= date('d.m.Y', strtotime($race->date_reg_to)) ?></strong></span>
            </div>
            <div class="info-text">
                <span><?= RaceHelper::statusSpecLabel($race->status,  $race->date_reg_from, $race->date_reg_to, $race->date_start,$race->date_end) ?></span>
            </div>
        </div>
    </div>
</div>
<?php if($products = $race->products){ ?>
    <div class="box">
        <div class="box-header with-border">Товары</div>
        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-bordered" style="margin-bottom: 0">
                    <thead>
                    <tr>
                        <th width="150" class="text-left">Изображение</th>
                        <th class="text-left">Название</th>
                        <th class="text-left">Стоимость</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($products as $product): ?>
                        <tr>
                            <td class="text-left">
                                <?= Html::img($product->getThumbFileUrl('photo', 'thumb'), ['class' => 'img-responsive']); ?>
                            </td>
                            <td class="text-left">
                                <?= $product->name ?>
                            </td>
                            <td class="text-left">
                                <?= "$product->price руб." ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
<?php } ?>
<h4>Участники</h4>
<ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#panel1">Общий зачёт</a></li>
    <li><a data-toggle="tab" href="#panel2">Мужчины</a></li>
    <li><a data-toggle="tab" href="#panel3">Женщины</a></li>
</ul>

<div class="tab-content">
    <div id="panel1" class="tab-pane fade in active">
        <?= $this->render('_users', [
            'model' => $race,
            'users' => $users->having(['sex'=>[Profile::SEX_FEMALE, Profile::SEX_MALE]])->all(),
        ]) ?>
    </div>
    <div id="panel2" class="tab-pane fade">
        <?= $this->render('_users', [
            'model' => $race,
            'users' => $users->having(['sex'=>Profile::SEX_MALE])->all(),
        ]) ?>
    </div>
    <div id="panel3" class="tab-pane fade">
        <?= $this->render('_users', [
            'model' => $race,
            'users' => $users->having(['sex'=> Profile::SEX_FEMALE])->all(),
        ]) ?>
    </div>
</div>




