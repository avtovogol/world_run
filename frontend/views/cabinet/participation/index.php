<?php
/**
 * @var $this \yii\web\View
 * @var $dataProvider \yii\data\ActiveDataProvider
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\grid\GridView;
use cabinet\entities\cabinet\Race;
use cabinet\helpers\RaceHelper;
use cabinet\helpers\OrderHelper;

$this->title= 'Мои участия';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="user-update">
    <div class="row">
        <div class="col-sm-12">
            <h3 style="margin-top: 0"><?= Html::encode($this->title) ?></h3>
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#panel1">Идёт регистрация</a></li>
                <li><a data-toggle="tab" href="#panel2">Идёт забег</a></li>
                <li><a data-toggle="tab" href="#panel3">Забег завершён</a></li>
            </ul>
            <div class="tab-content">
                <div id="panel1" class="tab-pane fade in active">
                    <?= $this->render('_items', [
                        'dataProvider' => $dataProviderStartReg
                    ]); ?>
                </div>
                <div id="panel2" class="tab-pane fade">
                    <?= $this->render('_items', [
                        'dataProvider' => $dataProviderStartDate
                    ]); ?>
                </div>
                <div id="panel3" class="tab-pane fade">
                    <?= $this->render('_items', [
                        'dataProvider' => $dataProviderFinished
                    ]); ?>
                </div>
            </div>

            <div style="margin-bottom: 20px" class="">
                <?= Html::a(Html::encode('Новое участие'),
                    Url::to(['all']),
                    ['class' => 'btn btn-success']
                ); ?>
            </div>
        </div>
    </div>

</div>
